package BS;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class BookStoreGUI extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					BookStoreGUI frame = new BookStoreGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public BookStoreGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnNewButton = new JButton("Sign in");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnNewButton.setBounds(312, 227, 89, 23);
		contentPane.add(btnNewButton);
		
		JLabel lblBookstore = new JLabel("Book-Store");
		lblBookstore.setFont(new Font("Times New Roman", Font.BOLD, 50));
		lblBookstore.setBounds(100, 25, 300, 60);
		contentPane.add(lblBookstore);
		
		JLabel lblBooks = new JLabel("Books");
		lblBooks.setFont(new Font("Times New Roman", Font.BOLD, 20));
		lblBooks.setBounds(30, 130, 70, 20);
		contentPane.add(lblBooks);
		
		JLabel lblMusicCds = new JLabel("Music CDs");
		lblMusicCds.setFont(new Font("Times New Roman", Font.BOLD, 20));
		lblMusicCds.setBounds(150, 130, 100, 20);
		contentPane.add(lblMusicCds);
		
		JLabel lblSoftwareCds = new JLabel("Software CDs");
		lblSoftwareCds.setFont(new Font("Times New Roman", Font.BOLD, 20));
		lblSoftwareCds.setBounds(280, 130, 150, 20);
		contentPane.add(lblSoftwareCds);
		
		JButton btnNewButton_1 = new JButton("Log in");
		btnNewButton_1.setBounds(53, 227, 89, 23);
		contentPane.add(btnNewButton_1);
	}

}
