-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 20, 2019 at 03:51 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bookstore`
--

-- --------------------------------------------------------

--
-- Table structure for table `bookdata`
--

CREATE TABLE `bookdata` (
  `BookId` int(11) NOT NULL,
  `BookName` varchar(50) NOT NULL,
  `ISBN` int(20) NOT NULL,
  `Author` varchar(50) NOT NULL,
  `Qty` int(11) NOT NULL,
  `Price` int(11) NOT NULL,
  `Status` enum('Available','Unavailable') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE `logs` (
  `Id` int(11) NOT NULL,
  `Created Time&Date` datetime NOT NULL,
  `CreatedBy` varchar(50) NOT NULL,
  `Modified Time&Date` datetime NOT NULL,
  `ModifiedBy` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `musiccddata`
--

CREATE TABLE `musiccddata` (
  `MusicCDId` int(11) NOT NULL,
  `MusicCDName` varchar(50) NOT NULL,
  `Volume` int(11) NOT NULL,
  `Director / Singer` varchar(50) NOT NULL,
  `Qty` int(11) NOT NULL,
  `Price` float NOT NULL,
  `Status` enum('Available','Unavailable') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `purchase`
--

CREATE TABLE `purchase` (
  `PurchaseNo` int(11) NOT NULL,
  `UserId` int(11) NOT NULL,
  `ProductId` enum('BookId','MusicCDId','SoftwareId') NOT NULL,
  `Date` date NOT NULL,
  `TotalAmount` float NOT NULL,
  `Status` enum('Paid','NotPaid') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `shoppingcard`
--

CREATE TABLE `shoppingcard` (
  `ShoppingNo` int(11) NOT NULL,
  `UserId` int(11) NOT NULL,
  `ProductId` int(11) NOT NULL,
  `Date&Time` datetime NOT NULL,
  `Qty` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `softwaredata`
--

CREATE TABLE `softwaredata` (
  `SoftwareId` int(11) NOT NULL,
  `SoftwareName` varchar(255) NOT NULL,
  `Version` varchar(50) NOT NULL,
  `Company` varchar(255) NOT NULL,
  `Qty` int(11) NOT NULL,
  `Price` float NOT NULL,
  `Status` enum('Available','Unavailable') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `userdata`
--

CREATE TABLE `userdata` (
  `UserId` int(11) NOT NULL,
  `UserName` varchar(50) NOT NULL,
  `Password` varchar(50) NOT NULL,
  `Address` varchar(255) NOT NULL,
  `UserStatus` enum('Active','Inactive') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bookdata`
--
ALTER TABLE `bookdata`
  ADD PRIMARY KEY (`BookId`);

--
-- Indexes for table `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `musiccddata`
--
ALTER TABLE `musiccddata`
  ADD PRIMARY KEY (`MusicCDId`);

--
-- Indexes for table `purchase`
--
ALTER TABLE `purchase`
  ADD PRIMARY KEY (`PurchaseNo`);

--
-- Indexes for table `shoppingcard`
--
ALTER TABLE `shoppingcard`
  ADD PRIMARY KEY (`ShoppingNo`);

--
-- Indexes for table `softwaredata`
--
ALTER TABLE `softwaredata`
  ADD PRIMARY KEY (`SoftwareId`);

--
-- Indexes for table `userdata`
--
ALTER TABLE `userdata`
  ADD PRIMARY KEY (`UserId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bookdata`
--
ALTER TABLE `bookdata`
  MODIFY `BookId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `logs`
--
ALTER TABLE `logs`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `musiccddata`
--
ALTER TABLE `musiccddata`
  MODIFY `MusicCDId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `purchase`
--
ALTER TABLE `purchase`
  MODIFY `PurchaseNo` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `shoppingcard`
--
ALTER TABLE `shoppingcard`
  MODIFY `ShoppingNo` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `softwaredata`
--
ALTER TABLE `softwaredata`
  MODIFY `SoftwareId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `userdata`
--
ALTER TABLE `userdata`
  MODIFY `UserId` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
bookdata