package BS;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import java.awt.SystemColor;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class LogInGUI extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LogInGUI frame = new LogInGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public LogInGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Customer Log In");
		lblNewLabel.setFont(new Font("Times New Roman", Font.BOLD, 30));
		lblNewLabel.setBounds(20, 10, 350, 40);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("User ID");
		lblNewLabel_1.setFont(new Font("Times New Roman", Font.BOLD, 15));
		lblNewLabel_1.setBounds(40, 71, 75, 20);
		contentPane.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("Password");
		lblNewLabel_2.setFont(new Font("Times New Roman", Font.BOLD, 15));
		lblNewLabel_2.setBounds(40, 96, 75, 20);
		contentPane.add(lblNewLabel_2);
		
		JTextArea textArea = new JTextArea();
		textArea.setBackground(SystemColor.textInactiveText);
		textArea.setBounds(125, 71, 75, 20);
		contentPane.add(textArea);
		
		JTextPane textPane = new JTextPane();
		textPane.setBackground(SystemColor.windowBorder);
		textPane.setFont(new Font("Tahoma", Font.PLAIN, 11));
		textPane.setBounds(125, 96, 75, 20);
		contentPane.add(textPane);
		
		JButton btnNewButton = new JButton("Log In");
		btnNewButton.setHorizontalAlignment(SwingConstants.RIGHT);
		btnNewButton.setBounds(315, 115, 70, 23);
		contentPane.add(btnNewButton);
		
		JLabel lblNewLabel_3 = new JLabel("User ID");
		lblNewLabel_3.setFont(new Font("Times New Roman", Font.BOLD, 15));
		lblNewLabel_3.setBounds(40, 153, 75, 20);
		contentPane.add(lblNewLabel_3);
		
		JLabel lblNewLabel_4 = new JLabel("Password");
		lblNewLabel_4.setFont(new Font("Times New Roman", Font.BOLD, 15));
		lblNewLabel_4.setBounds(40, 176, 75, 20);
		contentPane.add(lblNewLabel_4);
		
		JTextPane textPane_1 = new JTextPane();
		textPane_1.setBackground(SystemColor.windowBorder);
		textPane_1.setBounds(140, 153, 75, 20);
		contentPane.add(textPane_1);
		
		JTextPane textPane_2 = new JTextPane();
		textPane_2.setBackground(SystemColor.windowBorder);
		textPane_2.setBounds(140, 176, 75, 20);
		contentPane.add(textPane_2);
		
		JLabel lblConfirmPs = new JLabel("Confirm - PS");
		lblConfirmPs.setFont(new Font("Times New Roman", Font.BOLD, 15));
		lblConfirmPs.setBounds(40, 199, 90, 20);
		contentPane.add(lblConfirmPs);
		
		JTextPane textPane_3 = new JTextPane();
		textPane_3.setBackground(SystemColor.windowBorder);
		textPane_3.setBounds(140, 199, 75, 20);
		contentPane.add(textPane_3);
		
		JButton btnSignIn = new JButton("Sign In");
		btnSignIn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnSignIn.setBounds(315, 227, 75, 23);
		contentPane.add(btnSignIn);
	}
}
